#include <iostream>
#include <random>
#include <limits>
#include <csignal>
#include <string>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <regex>
#include <mutex>

#include <httplib.h>
#include <spdlog/spdlog.h>
#include <nlohmann/json.hpp>

#include <game.h>
#include <config.h>

using json = nlohmann::json;
namespace fs = std::filesystem;

httplib::Server srv;

void stop_server(int signal) {
    spdlog::info("received signal {}, stopping server...", signal);
    srv.stop();
}

int main(int argc, char** argv) {
    srv.set_exception_handler([](const httplib::Request& req, httplib::Response& resp, const std::exception& error){
        uint64_t error_id;

        static std::random_device dev;
        static std::mt19937 rng(dev());
        static std::uniform_int_distribution<std::mt19937::result_type>
            dist(0, std::numeric_limits<typeof(error_id)>::max());
        error_id = dist(rng);

        spdlog::error("error id {}: {}", error_id, error.what());

        resp.status = 500;
        resp.set_content("Error 500; please report this with the code " + std::to_string(error_id), "text/plain");
    });

    srv.set_post_routing_handler([](const auto& req, auto& res) {
        // development only
        //res.set_header("Access-Control-Allow-Origin", "*");
        //res.set_header("Access-Control-Allow-Headers", "Content-Type"); // needed for POST with json content
    });

    srv.Get("/(index(\\.html)?)?", [](const httplib::Request& req, httplib::Response& resp){
        static bool is_cached = false;
        static std::string content;

        if (!is_cached) {
            // note: no mutex, two threads will only overwrite each other - no harm done
            spdlog::info("reading index.html into cache");
            std::ifstream index_file(WAB_WWWDIR "/index.html");
            std::stringstream ss;
            if (!index_file.is_open()) {
                throw std::runtime_error("index file could not be read");
            }
            ss << index_file.rdbuf();
            content = ss.str();
            
            spdlog::info("index.html cached {} bytes", content.size());
            is_cached = true;
        }

        resp.set_content(content, "text/html");
    });

    srv.Get("/favicon.ico", [](const httplib::Request& req, httplib::Response& resp){
        resp.set_redirect("/static/img/favicon.ico", 301);
    });
    
    srv.Get("/static/(.+)?", [](const httplib::Request& req, httplib::Response& resp){
        static std::map<fs::path, std::string> static_cache;
        static std::map<fs::path, std::string> static_cache_mime;
        static std::mutex cache_lock;

        // construct filesystem path -> leading / kills all relative dirs
        fs::path requested = fs::path(std::string("/") + std::string(req.matches[1])).lexically_normal();

        if (static_cache.find(requested) == static_cache.end()) {
            std::lock_guard<std::mutex> guard(cache_lock);

            // not in cache -> insert
            fs::path requested_file = fs::path(WAB_WWWDIR) / "static" / fs::path(req.matches[1]).lexically_proximate("/");
            std::ifstream file(requested_file);
            if (!file.is_open()) {
                spdlog::warn("static file 404: {}", requested.native());
                resp.status = 404;
                resp.set_content("not found", "text/plain");
                return;
            }

            std::stringstream ss;
            ss << file.rdbuf();
            static_cache[requested] = ss.str();

            // guess mime type
            const std::regex file_end_regex("^.*[.](.+)$");
            std::smatch m;
            if (std::regex_match(requested.filename().native(), m, file_end_regex)) {
                const std::map<std::string, std::string> mime_by_fileending = {
                    {"js", "text/javascript"},
                    {"css", "text/css"},
                    {"png", "image/png"},
                    {"svg", "image/svg+xml"},
                    {"jpg", "image/jpeg"},
                    {"map", "application/json"},
                };

                if (mime_by_fileending.find(m[1]) != mime_by_fileending.end()) {
                    static_cache_mime[requested] = mime_by_fileending.at(m[1]);
                } else {
                    spdlog::warn("unkown file extension: {}", std::string(m[1]));
                    static_cache_mime[requested] = "text/plain";
                }
            } else {
                static_cache_mime[requested] = "text/plain";
            }

            spdlog::info("loaded {} in cache ({} bytes)", requested.native(), static_cache[requested].size());
        }
        
        resp.set_header("Cache-Control", "max-age=1800, public");
        resp.set_content(static_cache[requested], static_cache_mime[requested].c_str());
    });

    srv.Post("/api/getGameID", [](const httplib::Request& req, httplib::Response& resp){
        json j;
        j["gameID"] = wab::game::get_new_id();
        resp.set_content(j.dump(), "application/json");
    });

    srv.Get("/api/isLeaderboard", [](const httplib::Request& req, httplib::Response& resp){
        if (!req.has_param("score")) {
            resp.status = 400;
            resp.set_content("required param: score", "text/plain");
            return;
        }

        int score = std::stoi(req.get_param_value("score"));
        int maybe_pos = wab::game::get_maybe_leaderboard_pos(score);
        json j;
        j["leaderboard"] = wab::game::get_leaderboard_name_by_score(score);
        j["pos"] = maybe_pos;
        j["possible_badges"] = wab::game::get_possible_badges(score, maybe_pos, false);
        resp.set_content(j.dump(), "application/json");
    });

    // CORS pre-flight request needs to return with HTTP 200 (and Access-Control-Allow-... headers)
    // to allow POST with JSON data
    srv.Options("/api/storeResult", [](const httplib::Request& req, httplib::Response& resp) {
        resp.status = 200;
    });
    srv.Post("/api/storeResult", [](const httplib::Request& req, httplib::Response& resp){
        json req_json = json::parse(req.body);
        int score = req_json["score"].get<int>();
        std::string gameid = req_json["gameID"].get<std::string>();
        std::string name = req_json["name"].get<std::string>();

        json j;
        j["leaderboard"] = wab::game::get_leaderboard_name_by_score(score);
        j["pos"] = wab::game::store_leaderboard(gameid, score, name);
        j["badges"] = wab::game::get_badges(gameid);
        resp.set_content(j.dump(), "application/json");
    });

    srv.Get("/api/getLeaderboards", [](const httplib::Request& req, httplib::Response& resp){
        json j = json::object();
        for (const auto& it : wab::game::get_leaderboards()) {
            json leaderboard = json::array();
            for (const auto & entry : it.second) {
                json jentry;
                jentry["name"] = entry.name;
                jentry["score"] = entry.score;
                jentry["pos"] = entry.pos;
                jentry["when"] = entry.human_when;
                leaderboard.push_back(jentry);
            }
            j[it.first] = leaderboard;
        }
        resp.set_content(j.dump(), "application/json");
    });

    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " CFGFILE" << std::endl;
        return EINVAL;
    }

    wab::cfg::cfgfile = argv[1];

    uint16_t port = 0;
    try {
        port = wab::get_cfg()["port"].get<uint16_t>();
    } catch (const std::exception&) {
        spdlog::error("Unable to get port from {}", wab::cfg::cfgfile);
        return EINVAL;
    }

    try {
        wab::cfg::dbfile = wab::get_cfg()["dbfile"].get<std::string>();
    } catch (const std::exception& e) {
        spdlog::error("Unable to read dbfile from {}: {}", wab::cfg::cfgfile, e.what());
        return EINVAL;
    }


    // note: there is a potential race condition here w/ stopping the server before it started,
    // but this will be fixed by just issuing the signal again
    // and a workaround for this race condition would be very complicated,
    // due to the blocking behavior of cpp httplib
    signal(SIGTERM, stop_server);
    signal(SIGINT, stop_server);
    signal(SIGABRT, stop_server);

    // note: launched with a thread pool
    // -> databases catches most of the possible race conditions

    spdlog::info("attaching listener to 0.0.0.0:{}", port);
    srv.listen("0.0.0.0", port);

    return EXIT_SUCCESS;
}
