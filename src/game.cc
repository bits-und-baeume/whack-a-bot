#include <game.h>

#include <chrono>
#include <random>
#include <string>
#include <stdexcept>
#include <fstream>
#include <vector>

#include <config.h>

#include <spdlog/spdlog.h>
#include <nlohmann/json.hpp>
#include <sqlite_modern_cpp.h>
#include <httplib.h>

using json = nlohmann::json;

std::string wab::cfg::cfgfile;
std::string wab::cfg::dbfile;

static size_t get_current_timestamp() {
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

std::vector<std::string> wab::game::get_possible_badges(int score, int leaderboard_pos, bool cheated) {
    if (cheated) {
        return {"badge_cheater"};
    }

    std::vector<std::string> badges;

    if (score > 10) {
        badges.push_back("badge_bot");
    }
    if (score < -10) {
        badges.push_back("badge_baum");
    }
    if (score > 0 && leaderboard_pos <= LEADERBOARD_LENGTH) {
        badges.push_back("badge_bot_leaderboard");
    }
    if (score < 0 && leaderboard_pos <= LEADERBOARD_LENGTH) {
        badges.push_back("badge_baum_leaderboard");
    }

    return badges;
}

static std::string get_random_string() {
    std::string const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static std::random_device dev;
    static std::mt19937 rng(dev());
    static std::uniform_int_distribution<std::mt19937::result_type>
        dist(0, chars.size() - 1);

    std::string str = "";
    for (int i = 0; i < 12; i++) {
        str += chars[dist(rng)];
    }
    return str;
}

static std::string get_human_delta(int time_utc) {
    int delta_s = get_current_timestamp() - time_utc;
    if (delta_s < 59) {
        return "gerade eben";
    }
    if (delta_s < 3600) {
        return "vor " + std::to_string(delta_s/60) + " min";
    }
    if (delta_s < 86400) {
        return "vor " + std::to_string(delta_s/3600) + " h";
    }
    return "vor " + std::to_string(delta_s/86400) + " d";
}

static void check_valid_gameid(sqlite::database& db, std::string gameid) {
    int cnt;
    db << "SELECT COUNT(*) FROM games WHERE gameid=?;"
       << gameid >> cnt;
    
    if (cnt != 1) {
        throw std::runtime_error("unkown gameid: " + gameid);
    }
}

static bool is_game_running(sqlite::database& db, std::string gameid) {
    int cnt;
    check_valid_gameid(db, gameid);
    db << "SELECT COUNT(*) FROM games WHERE gameid=? AND end_time IS NULL;"
       << gameid >> cnt;
    return 1 == cnt;
}

sqlite::database wab::get_db() {
    static sqlite::database db(wab::cfg::dbfile);
    static bool setup_ran = false;
    if (!setup_ran) {
        spdlog::info("setting up db");
        db << "CREATE TABLE IF NOT EXISTS games("
              "  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
              "  gameid TEXT,"
              "  name TEXT,"
              "  score INT,"
              "  start_time INT,"
              "  end_time INT,"
              "  cheated INT,"
              "  badge_bot TEXT,"
              "  badge_baum TEXT,"
              "  badge_baum_leaderboard TEXT,"
              "  badge_bot_leaderboard TEXT,"
              "  badge_cheater TEXT"
              ");";
        db << "CREATE VIEW IF NOT EXISTS leaderboard AS "
            "SELECT * FROM games "
            "WHERE end_time > (CAST(strftime('%s', 'now') AS INT) - " STR(LEADERBOARD_CUTOFF_SEC) ") "
            "AND (cheated=FALSE "
            "OR end_time > (CAST(strftime('%s', 'now') AS INT) - " STR(CHEATER_TIMEOUT_SEC) "));";
        setup_ran = true;
    }

    return db;
}

std::string wab::game::get_new_id() {
    auto db = wab::get_db();
    db << "INSERT INTO games (start_time)VALUES(?)"
       << get_current_timestamp();
    auto internal_id = db.last_insert_rowid();
    std::string gameid = get_random_string() + std::to_string(internal_id);
    db << "UPDATE games SET gameid=? WHERE id=?;"
       << gameid
       << internal_id;

    return gameid;
}

static size_t get_cheat_cutoff() {
    return get_current_timestamp() - CHEATER_TIMEOUT_SEC;
}

static int get_pos(int score, int end) {
    auto db = wab::get_db();

    int people_before_this;
    std::string comparator = "";
    if (score < 0) {
        // user hit baum
        comparator = "<=";
    } else {
        // user hit bot (or did nothing)
        comparator = ">=";
    }
    db << ("SELECT COUNT(*) AS cnt FROM leaderboard WHERE score " + comparator + " ? AND end_time <= ? AND (end_time >= ? OR cheated=FALSE)")
       << score
       << end
       << get_cheat_cutoff()
       >> people_before_this;
    // current position
    return people_before_this + 1;
}

int wab::game::get_maybe_leaderboard_pos(int score) {
    return get_pos(score, get_current_timestamp()+10);
}

std::string wab::game::get_leaderboard_name_by_score(int score) {
    if (score < 0) {
        return "baum";
    }
    return "bot";
}

int get_game_start(sqlite::database& db, std::string gameid) {
    size_t res;
    check_valid_gameid(db, gameid);
    db << "SELECT start_time FROM games WHERE gameid=?"
       << gameid
       >> res;
    return res;
}

int wab::game::store_leaderboard(std::string gameid, int score, std::string name) {
    auto db = wab::get_db();
    check_valid_gameid(db, gameid);

    if (!is_game_running(db, gameid)) {
        throw std::runtime_error("game result for game " + gameid + " already stored");
    }

    size_t end = get_current_timestamp();
    size_t start = get_game_start(db, gameid);
    bool is_cheated = false;
    if (score > 60 || score < -60) {
        is_cheated = true;
    }
    if ((end - start) < 30) {
        is_cheated = true;
    }
    if (is_cheated) {
        spdlog::warn("game {} (user {}) probably cheated with a score of {} in {} s :c", gameid, name, score, end-start);
    }
    
    db << "UPDATE games SET score=?, name=?, end_time=?, cheated=? WHERE gameid=?;"
       << score
       << name
       << end
       << is_cheated
       << gameid;

    // already counts the just-created entry -> subtract that
    int pos = get_pos(score, end) - 1;

    // assign badges
    for (const auto& badge : wab::game::get_possible_badges(score, pos, is_cheated)) {
        try {
            db << ("UPDATE games SET " + badge + "=? WHERE gameid=?;")
               << get_badge_redeemcode(badge)
               << gameid;
        
        } catch (const std::exception& e) {
            spdlog::error("could not create badge {}: {}", badge, e.what());
        }
    }

    return pos;
}

std::map<std::string, std::vector<wab::leaderboard_entry>> wab::game::get_leaderboards() {
    std::vector<wab::leaderboard_entry> leader_bot, leader_baum;
    auto db = wab::get_db();
    db << "SELECT name, score, end_time FROM leaderboard WHERE score >= 0 ORDER BY score DESC, end_time ASC LIMIT ?;"
       << LEADERBOARD_LENGTH
       >> [&](std::string name, int score, int end_time) {
           wab::leaderboard_entry e;
           e.name = name;
           e.score = score;
           e.human_when = get_human_delta(end_time);
           e.pos = 1 + leader_bot.size();
           leader_bot.push_back(e);
       };
    db << "SELECT name, score, end_time FROM leaderboard WHERE score < 0 ORDER BY score ASC, end_time ASC LIMIT ?;"
       << LEADERBOARD_LENGTH
       >> [&](std::string name, int score, int end_time) {
           wab::leaderboard_entry e;
           e.name = name;
           e.score = score;
           e.human_when = get_human_delta(end_time);
           e.pos = 1 + leader_baum.size();
           leader_baum.push_back(e);
       };

    return {
        {"bot", leader_bot},
        {"baum", leader_baum},
    };
}

std::map<std::string, std::string> wab::game::get_badges(std::string gameid) {
    const std::vector<std::string> all_badges = {
        "badge_bot",
        "badge_baum",
        "badge_baum_leaderboard",
        "badge_bot_leaderboard",
        "badge_cheater",
    };

    std::map<std::string, std::string> codes;

    auto db = wab::get_db();
    for (const auto& badge : all_badges) {
        std::string redeemcode;
        db << ("SELECT " + badge + " FROM games WHERE gameid=?")
           << gameid
           >> redeemcode;

        if (!redeemcode.empty()) {
            codes[badge] = redeemcode;
        }
    }

    return codes;
}

json wab::get_cfg() {
    static bool loaded = false;
    static json j;
    if (!loaded) {
        spdlog::info("loading cfg...");
        std::ifstream i(wab::cfg::cfgfile);
        if (!i.is_open()) {
            spdlog::error("could not open file: {}", wab::cfg::cfgfile);
            throw std::runtime_error("could not read cfgfile");
        }
        i >> j;
        loaded = true;
    }
    return j;
}

std::string wab::get_badge_redeemcode(std::string badgename) {
    json badges = get_cfg()["badges"];
    std::string api_server = get_cfg()["badges_api_server"];
    std::string api_path = get_cfg()["badges_api_path"];

    if (badges.find(badgename) == badges.end()) {
        throw std::runtime_error("badge does not exist: " + badgename);
    }

    json req_body = json::object();
    req_body["badge_class"] = "single";
    req_body["issuing_token"] = badges[badgename];
    std::string req_body_str = req_body.dump();

    httplib::Client client(api_server);
    auto response = client.Post(api_path.c_str(), req_body_str.c_str(), "application/json");

    if (200 != response->status) {
        spdlog::error("could not retrieve badge from api, code: {}, body: {}", response->status, response->body);
        throw std::runtime_error("could not retrieve redeemcode from api");
    }

    spdlog::info("retrieved one-time redeemcode for {} (code: {})", badgename, response->status);
    json redeemcode = json::parse(response->body);
    // implicitly casted to string
    return redeemcode;
}
