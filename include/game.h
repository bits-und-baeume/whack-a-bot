#pragma once

#include <string>
#include <map>
#include <vector>

#include <nlohmann/json.hpp>
#include <sqlite_modern_cpp.h>

using json = nlohmann::json;

namespace wab {
    sqlite::database get_db();
    
    json get_cfg();

    namespace cfg {
        extern std::string cfgfile;
        extern std::string dbfile;
    };

    struct leaderboard_entry {
        std::string name;
        int score;
        int pos;
        std::string human_when;
    };
    typedef struct leaderboard_entry leaderboard_entry;

    std::string get_badge_redeemcode(std::string badgename);

    namespace game {
        std::string get_new_id();
        int get_maybe_leaderboard_pos(int score);
        std::string get_leaderboard_name_by_score(int score);
        int store_leaderboard(std::string gameid, int score, std::string name);
        std::map<std::string, std::vector<leaderboard_entry>> get_leaderboards();
        std::vector<std::string> get_possible_badges(int score, int leaderboard_pos, bool cheated);
        std::map<std::string, std::string> get_badges(std::string gameid);
    };
}
