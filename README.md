simple build recipe

```
nix-build
nix-shell -p ./result --run './result/bin/wab ./etc/wab.json.example'
```

pls copy the `.json` file before adjusting it.

License: [AGPL v3+](./COPYING)
