{ pkgs ? import <nixpkgs> {},
  stdenv ? pkgs.stdenv}:
let
  cpp-httplib = stdenv.mkDerivation rec {
    pname = "cpp-httplib";
    version = "0.9.9";

    src = pkgs.fetchurl {
      url = "https://github.com/yhirose/${pname}/archive/refs/tags/v${version}.tar.gz";
      sha256 = "13p0wdl0d4ri834fnzxzkm9hi37g4pmc64shv2dvnlyz80cfllbd";
    };
    dontUseMesonConfigure = true;

    propagatedBuildInputs = with pkgs; [zlib openssl];
    buildInputs = with pkgs; [cmake python3];

    cmakeFlags = [
      "-DBUILD_SHARED_LIBS=OFF"
      "-DHTTPLIB_USE_OPENSSL_IF_AVAILABLE=ON"
      "-DHTTPLIB_USE_ZLIB_IF_AVAILABLE=ON"
      "-DHTTPLIB_REQUIRE_OPENSSL=ON"
      "-DHTTPLIB_REQUIRE_ZLIB=ON"
      "-DHTTPLIB_USE_BROTLI_IF_AVAILABLE=OFF"
      "-DHTTPLIB_REQUIRE_BROTLI=OFF"
      "-DHTTPLIB_COMPILE=ON"
    ];
  };

  sqlite_modern_cpp = stdenv.mkDerivation rec {
    pname = "sqlite_modern_cpp";
    version = "3.2";

    src = pkgs.fetchurl {
      url = "https://github.com/SqliteModernCpp/${pname}/archive/refs/tags/v${version}.tar.gz";
      sha256 = "0x3dk2giybsc51cy0rwfaa75n3a8gm8baq22r3gllizgq2118x3a";
    };

    propagatedBuildInputs = with pkgs; [sqlite];
    buildInputs = with pkgs; [];
  };
in
stdenv.mkDerivation rec {
  name = "wab";
  version = "0.0.1";

  src = ./.;

  buildInputs = with pkgs; [
    # build stuff
    cmake
    cmakeCurses
    stdenv.cc

    # libraries
    cpp-httplib
    spdlog
    nlohmann_json
    sqlite_modern_cpp
  ];
}
